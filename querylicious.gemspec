# frozen-string-literal: true

Gem::Specification.new do |spec|
  version_file = File.expand_path('VERSION', __dir__)
  version = File.read(version_file).lines.first.chomp

  spec.name          = 'querylicious'
  spec.version       = version.split('+').first
  spec.authors       = ['Jo-Herman Haugholt']
  spec.email         = ['johannes@huyderman.com']

  spec.summary       = 'An opinionated search query parser'
  spec.homepage      = 'https://gitlab.com/huyderman/querylicious'
  spec.license       = 'MIT'
  spec.metadata = {
    'documentation_uri' => 'https://gitlab.com/huyderman/querylicious/blob/master/README.md',
    'changelog_uri' => 'https://gitlab.com/huyderman/querylicious/blob/master/CHANGELOG.md',
    'bug_tracker_uri' => 'https://gitlab.com/huyderman/querylicious/issues'
  }

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.require_paths = %w[lib]

  spec.required_ruby_version = '>= 2.5'

  spec.add_runtime_dependency 'dry-initializer', '~> 3.0'
  spec.add_runtime_dependency 'dry-matcher', '>= 0.6.0', '< 1.1'
  spec.add_runtime_dependency 'dry-struct', '~> 1.0'
  spec.add_runtime_dependency 'dry-types', '~> 1.0'
  spec.add_runtime_dependency 'parslet', '~> 2.0'

  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'mutant', '~> 0.11.0'
  spec.add_development_dependency 'mutant-rspec', '~> 0.11.0'
  spec.add_development_dependency 'pry', '~> 0.11'
  spec.add_development_dependency 'rake', '~> 13.0'
  spec.add_development_dependency 'reek', '~> 6.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'rspec_junit_formatter', '~> 0.6.0'
  spec.add_development_dependency 'rspec-parameterized', '~> 0.5.0'
  spec.add_development_dependency 'rubocop', '~> 1.39'
  spec.add_development_dependency 'rubocop-performance', '~> 1.15'
  spec.add_development_dependency 'simplecov', '~> 0.21.0'
end
